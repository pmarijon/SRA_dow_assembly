import os
import sys
import json
import logging
import argparse
import requests
import itertools
import subprocess
import statistics
import multiprocessing

from pathlib import Path
from bs4 import BeautifulSoup

canu_call_template = "canu -p canu -pacbio-raw {} -d {} genomeSize={} correctedErrorRate={} corOutCoverage={} corMinCoverage={} maxThreads=15"
canu_call_template_unitig = canu_call_template + " stopAfter=unitig"

minimap_call_template = "minimap -x ava10k -t 10 {} {}"
miniasm_call_template = "miniasm -f {} {}"

base_dir = os.path.dirname(__file__) + os.sep
bact_dict = json.load(open(base_dir+'data'+ os.sep + 'NCTC.json'))

def nb_base_in_file(path: Path):
    nb_base = 0

    path = os.path.abspath(str(path))
    with open(path, "r") as sequences:
        line_count = 0
        for line in sequences:
            line_count += 1
            if line_count % 4 == 2: # take second line each 4 line
                nb_base += len(line)

    return nb_base

def main(args = None):
    
    if args is None:
        args = sys.argv[1:]

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s :: %(message)s')
    steam_handler = logging.StreamHandler()
    steam_handler.setFormatter(formatter)
    logger.addHandler(steam_handler)

    parser = argparse.ArgumentParser(prog="assembly_report",
                                     formatter_class=argparse.
                                     ArgumentDefaultsHelpFormatter)
    subparser = parser.add_subparsers()

    parser.add_argument("-i", "--input",
                        help="File contain on SRA accession sample per line",
                        type=argparse.FileType("r"), required=True)
    parser.add_argument("-d", "--data",
                        help="directory where sequence are download",
                        type=Path, required=True)
    parser.add_argument("-a", "--assembly",
                        help="directory where assembly are save",
                        type=Path, required=True)
    
    subparser_select = subparser.add_parser('select', help="Partial assembly first run")
    subparser_select.set_defaults(func=select)

    subparser_finish = subparser.add_parser('finish', help="Finish canu assembly and run mini{map|asm}")
    subparser_finish.set_defaults(func=finish)

    args = vars(parser.parse_args(args))
    if "func" not in args:
        print("You forget task specify select or finish")
        parser.print_help()
        return 1

    args["func"](args)

    return 0

def finish(args):
    for accession in args["input"]:
        print(accession)
        accession, ERS_accession = accession.strip().split()
        
        logging.info("begin of get information for "+accession)
        run_id, assembly_len = get_info_about_ERS(ERS_accession)
        logging.info("end of get information for "+accession)
       
        if not assembly_len:
            logging.error("We didn't find any assembly len, we skip this !")
            continue

        data_path = args["data"] / accession
        data_path.mkdir(parents=True, exist_ok=True)
        
        logging.info("begin of compute canu parametre for "+accession)
        fastq_files, *canu_parameters = compute_canu_parameter(data_path, assembly_len)
        logging.info("end of compute canu parametre for "+accession)

        if not fastq_files:
            logging.error("We didn't find any file in data directory, we skip this !")
            continue

        assembly_path = args["assembly"] / accession / "canu"
        assembly_path.mkdir(parents=True, exist_ok=True)

        logging.info("begin finish canu assembly")
        canu_call = canu_call_template.format(fastq_files,
                                              os.path.abspath(assembly_path),
                                              assembly_len, *canu_parameters)
        logging.info(canu_call)
        logname = os.path.abspath(str(assembly_path / "canu_log_finish"))
        with open(logname+".out", "w") as outfile:
            pass
            out = subprocess.call(canu_call.split(" "), stdout=outfile,
                                  stderr=subprocess.STDOUT,
                                  universal_newlines=True)
        logging.info("end finish canu assembly")

        minimap_path = args["assembly"] / accession / "minimap"
        minimap_path.mkdir(parents=True, exist_ok=True)

        logging.info("begin minimap")
        minimap_call = minimap_call_template.format(fastq_files, fastq_files)
        minimap_out = str(minimap_path / "minimap.paf") 
        minimap_log = str(minimap_path / "minimap.log")
       
        logging.info(minimap_call)
        
        with open(minimap_log, "w") as errfile:
            with open(minimap_out, "w") as outfile:
                pass
                out = subprocess.call(minimap_call.split(" "),
                                      stdout=outfile,
                                      stderr=errfile,
                                      universal_newlines=True)
        logging.info("end minimap")

        miniasm_path = args["assembly"] / accession / "miniasm"
        miniasm_path.mkdir(parents=True, exist_ok=True)
        
        logging.info("begin miniasm")
        miniasm_call = miniasm_call_template.format(fastq_files, minimap_out)
        miniasm_out = str(miniasm_path / "miniasm.gfa") 
        miniasm_log = str(miniasm_path / "miniasm.log")
       
        logging.info(miniasm_call)
        
        with open(miniasm_log, "w") as errfile:
            with open(miniasm_out, "w") as outfile:
                pass
                out = subprocess.call(miniasm_call.split(" "),
                                      stdout=outfile,
                                      stderr=errfile,
                                      universal_newlines=True)
        logging.info("end miniasm")

def select(args):
    for accession in args["input"]:
        accession, ERS_accession = accession.strip().split()

        data_path = args["data"] / accession
        
        logging.info("begin of download data for "+accession)
        run_id, assembly_len = get_info_about_ERS(ERS_accession)
        download_extract_merge(data_path, accession)
        logging.info("end of download data for "+accession)
        
        if not assembly_len:
            logging.error("We didn't find any assembly len, we skip this !")
            continue

        # compute coverage
        logging.info("begin of compute canu parametre for "+accession)
        fastq_files, *canu_parameters = compute_canu_parameter(data_path, assembly_len)
        logging.info("end of compute canu parametre for "+accession)

        logging.info("begin of canu assembly "+accession)

        assembly_path = args["assembly"] / accession / "canu"
        assembly_path.mkdir(parents=True, exist_ok=True)
        
        canu_call = canu_call_template_unitig.format(fastq_files,
                                                     os.path.abspath(assembly_path),
                                                     assembly_len, *canu_parameters)
        logging.info(canu_call)
        logname = os.path.abspath(str(assembly_path / "canu_log_unitig"))
        with open(logname+".out", "w") as outfile:
            out = subprocess.call(canu_call.split(" "), stdout=outfile,
                                  stderr=subprocess.STDOUT,
                                  universal_newlines=True)

        logging.info("end of canu assembly "+accession)

base_request = "https://www.ebi.ac.uk/ena/data/view/"
info_request = base_request + "{}&display=xml"
assembly_request = base_request + "Taxon:{}&display=xml&portal=assembly"

def get_info_about_ERS(accession):
    # find taxon id and run id
    r = requests.get(info_request.format(accession))
    soup = BeautifulSoup(r.text, "xml")

    for db in soup.find_all():
        if db.get_text() == "ENA-RUN":
            run_id = db.parent.ID.get_text().strip().replace(",", " ")
    
    taxon_id = soup.find("TAXON_ID").get_text()
    r = requests.get(assembly_request.format(taxon_id))
    soup = BeautifulSoup(r.text, "xml")
    list_of_size = [int(attr.parent.VALUE.get_text())
                    for attr in soup.find_all("TAG")
                    if attr.get_text() == "total-length"]
    if len(list_of_size) == 0:
        return None, None
    
    assembly_len = statistics.mean(list_of_size)

    return run_id, assembly_len

def compute_canu_parameter(data_path, assembly_len):
    nb_base = 0
    fastq_file = data_path / "merged.fasta"
    nb_base = nb_base_in_file(str(fastq_file))

    coverage = nb_base / assembly_len 

    # run assembly
    correctedErrorRate = 0.045
    if coverage >= 60:
        correctedErrorRate = 0.105
    if coverage <= 30:
        correctedErrorRate = 0.040

    corOutCoverage = 40 
    if coverage < 40 :
        corOutCoverage = int(coverage)
    if corOutCoverage < 1.0:
        corOutCoverage = 1.0

    corMinCoverage = 4
    if coverage < 30 :
        corMinCoverage = 0

    return fastq_file, correctedErrorRate, corOutCoverage, corMinCoverage 

ftp_url = "ftp://ftp.sra.ebi.ac.uk//vol1/"

def download_extract_merge(data_path, NCTC_id):
    
    data_path.mkdir(parents=True, exist_ok=True)
    
    logname = os.path.abspath(str(data_path / "wget_log"))
    bax_files = list()
    job_list = list()
    with open(logname+".out", "w") as outfile:
        for file_path in itertools.chain.from_iterable(bact_dict[NCTC_id]["file_paths"].values()):
            if file_path.endswith(".bax.h5"):
                bax_files.append(str(data_path / os.path.basename(file_path)))

            cmd = ["wget", "-N", ftp_url + file_path, "-P", str(data_path)]
            job_list.append(cmd)
    
    p = multiprocessing.Pool()
    p.map(run_paralelle, job_list)

    dextract_cmd = ["dextract", "-o"] + bax_files
    logname = os.path.abspath(str(data_path / "dextract_log"))
    with open(logname+".err", "w") as errfile, open(str(data_path / "merged.fasta"), "w") as outfile:
            subprocess.call(dextract_cmd, stdout=outfile, stderr=errfile,
                            universal_newlines=True)
        
def run_paralelle(cmd):
    logging.info("run " + " ".join(cmd))
    subprocess.call(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL,
                    universal_newlines=True)
    logging.info("end " + " ".join(cmd))

def fastq_file(data_path):
    for child in data_path.iterdir():
        if child.suffix == ".fasta":
            yield str(child)

if __name__ == "__main__":

    sys.exit(main(sys.argv[1:]))
