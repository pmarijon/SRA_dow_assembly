#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

from SRA_dow_assembly import __version__, __name__

from pip.req import parse_requirements

install_reqs = parse_requirements("requirements.txt", session=False)
reqs = [str(ir.req) for ir in install_reqs]

setup(
    name= __name__,
    version= __version__,
    packages = find_packages(),

    author="Pierre Marijon",
    author_email="pierre.marijon@inria.fr",
    description="SRA_dow_assembly",
    long_description=open('README.md').read(),
    url="https://gitlab.inria.fr/pmarijon/SRA_dow_assembly",
    
    install_requires = reqs,
    include_package_data=True,
    
    classifiers=[
        "Programming Language :: Python :: 3",
        "Development Status :: 2 - Pre-Alpha"
    ],

    entry_points = {
        'console_scripts': [
            'SRA_dow_assembly = SRA_dow_assembly.__main__:main'
        ]
    }
)
